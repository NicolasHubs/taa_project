#!/bin/bash

access_token=$(curl -X POST \
    -H 'content-type: application/x-www-form-urlencoded' \
    -d 'username=emorel&password=1234&grant_type=password' \
    -d 'client_id=quarkus-app' \
    -d 'client_secret=**********' \
    "http://localhost:8180/auth/realms/quarkus-realm/protocol/openid-connect/token" | jq --raw-output '.access_token')

echo ${access_token}

curl -v -X GET \
  http://localhost:8080/api/user \
  -H "Authorization: Bearer ${access_token}" 

