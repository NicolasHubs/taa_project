if [[ -z "${GRAALVM_HOME}" ]]; then
	export GRAALVM_HOME=$HOME/Development/graalvm/
fi
docker-compose down --remove-orphans
docker-compose up --build
