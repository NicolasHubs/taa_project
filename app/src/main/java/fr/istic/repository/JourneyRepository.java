package fr.istic.repository;

import fr.istic.domain.Journey;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class JourneyRepository implements PanacheRepository<Journey> {

    public Journey findByStartAndDestination(String start, String destination){
        return find("start = ?1 and destination = ?2", start, destination).firstResult();
    }

    public void save(Journey journey){
        journey.persist();
    }
}
