package fr.istic.repository;

import fr.istic.domain.Travel;
import fr.istic.domain.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Transactional
public class TravelRepository implements PanacheRepository<Travel> {

    @Inject
    UserRepository userRepository;

    public List<Travel> findByAdresseMail(String adresseMail) {
        List<Travel> travels = new ArrayList<>();
        User user = userRepository.findByAdresseMail(adresseMail);
        if (user != null) {
            travels = new ArrayList<>(user.getTravels());
        }
        return travels;
    }

    public void save(Travel travel) {
        travel.persist();
    }
}
