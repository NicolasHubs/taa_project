package fr.istic.repository;

import fr.istic.domain.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class UserRepository implements PanacheRepository<User> {

    // put your custom logic here as instance methods
    public User findByAdresseMail(String adresseMail){
        return find("adresseMail", adresseMail).firstResult();
    }

    public void saveOrUpdate(User user){
        User oldUser = this.findByAdresseMail(user.getAdresseMail());
        if(oldUser != null) {
            if (user.getWorkAddress() != null)
                oldUser.setWorkAddress(user.getWorkAddress());
            if (user.getFavoriteConveyance() != null)
                oldUser.setFavoriteConveyance(user.getFavoriteConveyance());
            if (user.getHomeAddress() != null)
                oldUser.setHomeAddress(user.getHomeAddress());
            if (user.getPseudo() != null)
                oldUser.setPseudo(user.getPseudo());
            oldUser.persist();
        } else {
            user.persist();
        }
    }
}