package fr.istic.mapper;


import java.util.List;
import org.mapstruct.Mapper;
import fr.istic.domain.User;
import fr.istic.dto.UserDTO;

@Mapper(config = QuarkusMappingConfig.class)
public interface UserMapper {

    UserDTO toDto(User user);

    List<UserDTO> usersToUserDtos(List<User> users);

    User toEntity(UserDTO userDTO);
}