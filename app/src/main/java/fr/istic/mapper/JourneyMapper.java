package fr.istic.mapper;

import fr.istic.domain.Journey;
import fr.istic.dto.JourneyDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper(config = QuarkusMappingConfig.class)
public interface JourneyMapper {

    @Mapping(target="from", source="journey.start")
    @Mapping(target="to", source="journey.destination")
    @Named("toDto")
    JourneyDTO toDto(Journey journey);

    @IterableMapping(qualifiedByName = "toDto")
    List<JourneyDTO> journeysToJourneyDtos(List<Journey> journeys);

    @Mapping(target="start", source="journeyDTO.from")
    @Mapping(target="destination", source="journeyDTO.to")
    Journey toEntity(JourneyDTO journeyDTO);
}