package fr.istic.mapper;

import fr.istic.domain.Travel;
import fr.istic.dto.TravelDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = QuarkusMappingConfig.class)
public interface TravelMapper {

    TravelDTO toDto(Travel travel);

    List<TravelDTO> travelsToTravelDtos(List<Travel> travels);

    Travel toEntity(TravelDTO travelDTO);
}