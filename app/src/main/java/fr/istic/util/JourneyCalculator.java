package fr.istic.util;

import fr.istic.dto.CoordonateDTO;
import fr.istic.dto.JourneyDTO;
import fr.istic.dto.RouteDTO;

public interface JourneyCalculator {

    RouteDTO calculJourney(JourneyDTO journey, String type);

    RouteDTO calculJourneyVoiture(JourneyDTO journey, CoordonateDTO coordonateDTO);

    RouteDTO calculJourneyVelo(JourneyDTO journey,CoordonateDTO coordonateDTO);

    RouteDTO calculJourneyTEC(JourneyDTO journey, CoordonateDTO coordonateDTO);

    RouteDTO calculJourneyPieton(JourneyDTO journey,CoordonateDTO coordonateDTO);
}
