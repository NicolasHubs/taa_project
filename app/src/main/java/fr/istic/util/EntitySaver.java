package fr.istic.util;

import fr.istic.dto.RouteDTO;
import fr.istic.dto.UserDTO;

public interface EntitySaver {
    void saveRoute(RouteDTO routeDTO, UserDTO userDTO);
}
