package fr.istic.util;

public class Constants {

    private Constants() {
    }

    public static final String GEO_SERVICE_API = "https://wxs.ign.fr/choisirgeoportail/itineraire/rest/route.json?";

    public static final String LOCATION_IQ_API = "https://eu1.locationiq.com/v1/search.php?key=e4559681aca0b1&format=json&";

    public static final String NAVITIA_API = "https://api.navitia.io/v1/coverage/fr-nw/journeys?";

    public static final String CYCLE_STREET_API = "https://www.cyclestreets.net/api/journey.json?key=e2f0fd14e2870330&plan=fastest&";

    public static final String NAVITIA_NAME = "Navitia";

    public static final String GEOSERVICE_NAME = "Geoservice";

    public static final String CYCLE_STREET_NAME = "cyclestreets";

    public static final String NAVITIA_TOKEN = "b204d83d-6769-4480-b79f-682f691727b0";

    public static final String FASTEST = "fastest";

    public static final String BEST = "best";

    public static final String ACCEPT = "Accept";

    public static final String PUBLIC_TRANSPORT = "public_transport";

    public static final String APPLICATION_JSON = "application/json";

    public static final String HTTP_ERROR = "Failed : HTTP error code : \n";

    public static final String HTTP_MALFORMED_URL = "Failed : Malformed URL : ";

    public static final String HTTP_CODE_NOT_200 = "HTTP response code not 200 !";

    public static final String NOT_PARSABLE = "Unable to parse duration : ";

    public static final String COMPLETE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_DATE = "1970-01-01 ";
}
