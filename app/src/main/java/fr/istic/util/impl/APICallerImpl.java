package fr.istic.util.impl;

import fr.istic.util.APICaller;
import fr.istic.util.Constants;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class APICallerImpl implements APICaller {

    Logger logger = Logger.getLogger(APICallerImpl.class.getName());

    @Override
    public JSONObject callGeoservice(String from, String to, String type) {

        URL url = null;

        try {
            url = new URL(Constants.GEO_SERVICE_API +
                    "origin=" + from + "&destination=" + to + "&graphName=" + type);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(Constants.ACCEPT, Constants.APPLICATION_JSON);

            logger.log(Level.INFO,"Geoservice call : {0}", url);

            conn.connect();

            if (conn.getResponseCode() != 200) {
                logger.severe(Constants.HTTP_CODE_NOT_200);
                logger.severe(Constants.HTTP_ERROR + conn.getResponseCode());
            }

            Scanner sc = new Scanner(conn.getInputStream());

            StringBuilder bld = new StringBuilder();
            while (sc.hasNext()) {
                bld.append(sc.nextLine());
            }

            String output = bld.toString();

            JSONObject obj = new JSONObject(output);

            sc.close();
            conn.disconnect();

            return obj;

        } catch (MalformedURLException e) {
            logger.severe(Constants.HTTP_MALFORMED_URL + url + "\n" + e.getMessage());
        } catch (IOException e) {
            logger.severe(Constants.HTTP_ERROR + e.getMessage());
        }
        return null;
    }

    @Override
    public JSONArray callLocationIQ(String adress) {

        URL url = null;

        try {
            adress = java.net.URLEncoder.encode(adress, "UTF-8");
            url = new URL(Constants.LOCATION_IQ_API +
                    "q=" + adress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(Constants.ACCEPT, Constants.APPLICATION_JSON);

            logger.log(Level.INFO,"LocationIQ call : {0}", url);

            conn.connect();

            if (conn.getResponseCode() != 200) {
                logger.severe(Constants.HTTP_CODE_NOT_200);
                logger.severe(Constants.HTTP_ERROR + conn.getResponseCode());
            }

            Scanner sc = new Scanner(conn.getInputStream());

            StringBuilder bld = new StringBuilder();
            while (sc.hasNext()) {
                bld.append(sc.nextLine());
            }

            String output = bld.toString();

            JSONArray obj = new JSONArray(output);

            sc.close();
            conn.disconnect();

            return obj;

        } catch (MalformedURLException e) {
            logger.severe(Constants.HTTP_MALFORMED_URL + url + "\n" + e.getMessage());
        } catch (IOException e) {
            throw new NotFoundException("Coordonates not found for adress : " + adress, e);
        }
        return null;
    }

    @Override
    public JSONObject callNavitia(String from, String to, String type, Instant instant) {

        URL url = null;

        Date myDate = Date.from(instant);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String formattedDate = formatter.format(myDate);

        try {
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(Constants.NAVITIA_TOKEN.getBytes()));
            url = new URL(Constants.NAVITIA_API +
                    "from=" + from + "&to=" + to + "&datetime_represents=departure" + "&datetime=" + formattedDate + ((type != null) ? "&traveler_type=" + type : ""));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(Constants.ACCEPT, Constants.APPLICATION_JSON);
            conn.setRequestProperty("Authorization", basicAuth);

            logger.log(Level.INFO,"Navitia call : {0}", url);

            conn.connect();

            if (conn.getResponseCode() != 200) {
                logger.severe(Constants.HTTP_CODE_NOT_200);
                logger.severe(Constants.HTTP_ERROR + conn.getResponseCode());
            }

            Scanner sc = new Scanner(conn.getInputStream());

            StringBuilder bld = new StringBuilder();
            while (sc.hasNext()) {
                bld.append(sc.nextLine());
            }

            String output = bld.toString();

            JSONObject obj = new JSONObject(output);

            sc.close();
            conn.disconnect();

            return obj;

        } catch (MalformedURLException e) {
            logger.severe(Constants.HTTP_MALFORMED_URL + url + "\n" + e.getMessage());
        } catch (IOException e) {
            logger.severe(Constants.HTTP_ERROR + e.getMessage());
        }
        return null;
    }

    @Override
    public JSONObject callCycleStreet(String from, String to) {

        URL url = null;

        try {
            url = new URL(Constants.CYCLE_STREET_API +
                    "speed=24&itinerarypoints=" + from + "|" + to);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(Constants.ACCEPT, Constants.APPLICATION_JSON);

            logger.log(Level.INFO,"CycleStreets call : {0}", url);

            conn.connect();

            if (conn.getResponseCode() != 200) {
                logger.severe(Constants.HTTP_CODE_NOT_200);
                logger.severe(Constants.HTTP_ERROR + conn.getResponseCode());
            }

            Scanner sc = new Scanner(conn.getInputStream());

            StringBuilder bld = new StringBuilder();
            while (sc.hasNext()) {
                bld.append(sc.nextLine());
            }

            String output = bld.toString();

            JSONObject obj = new JSONObject(output);

            sc.close();
            conn.disconnect();

            return obj;

        } catch (MalformedURLException e) {
            logger.severe(Constants.HTTP_MALFORMED_URL + url + "\n" + e.getMessage());
        } catch (IOException e) {
            logger.severe(Constants.HTTP_ERROR + e.getMessage());
        }
        return null;
    }

}
