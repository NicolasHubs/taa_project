package fr.istic.util.impl;

import fr.istic.dto.CoordonateDTO;
import fr.istic.dto.JourneyDTO;
import fr.istic.dto.RouteDTO;
import fr.istic.dto.StepDTO;
import fr.istic.util.APICaller;
import fr.istic.util.Constants;
import fr.istic.util.JourneyCalculator;
import fr.istic.util.TypeEnum;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@ApplicationScoped
public class JourneyCalculatorImpl implements JourneyCalculator {

    Logger logger = Logger.getLogger(JourneyCalculatorImpl.class.getName());

    @Inject
    APICaller apiCaller;

    @Override
    public RouteDTO calculJourney(JourneyDTO journey, String type) {
        RouteDTO routeDTO = new RouteDTO();
        try {
            Map<String, CoordonateDTO> coordonateDTOMap = getCoordonates(journey);
            if (type.equals(TypeEnum.PIETON.getKey())) {
                routeDTO = calculJourneyPieton(journey, coordonateDTOMap.get(Constants.GEOSERVICE_NAME));
            } else if (type.equals(TypeEnum.VOITURE.getKey())) {
                routeDTO = calculJourneyVoiture(journey, coordonateDTOMap.get(Constants.GEOSERVICE_NAME));
            } else if (type.equals(TypeEnum.TRANSPORT_EN_COMMUNS.getKey())) {
                routeDTO = calculJourneyTEC(journey, coordonateDTOMap.get(Constants.NAVITIA_NAME));
            } else if (type.equals(TypeEnum.VELO.getKey())) {
                routeDTO = calculJourneyVelo(journey, coordonateDTOMap.get(Constants.GEOSERVICE_NAME));
            }
        } catch (NotFoundException e) {
            throw new NotFoundException(e);
        }
        return routeDTO;
    }

    @Override
    public RouteDTO calculJourneyVelo(JourneyDTO journeyDTO, CoordonateDTO coordonateDTO) {
        JSONObject response = apiCaller.callCycleStreet(coordonateDTO.getCoordonateFrom(), coordonateDTO.getCoordonateTo());
        RouteDTO routeDTO = getCycleStreetRoute(response);
        routeDTO.setType(TypeEnum.VELO);
        routeDTO.setFrom(journeyDTO.getFrom());
        routeDTO.setTo(journeyDTO.getTo());
        routeDTO.setCoDeuxEmmission(0.009);
        return routeDTO;
    }

    @Override
    public RouteDTO calculJourneyTEC(JourneyDTO journeyDTO, CoordonateDTO coordonateDTO) {
        Instant now = Instant.now();
        JSONObject response = apiCaller.callNavitia(coordonateDTO.getCoordonateFrom(), coordonateDTO.getCoordonateTo(), null, now);
        RouteDTO routeDTO = getNavitiaRoute(response);
        routeDTO.setType(TypeEnum.TRANSPORT_EN_COMMUNS);
        routeDTO.setFrom(journeyDTO.getFrom());
        routeDTO.setTo(journeyDTO.getTo());
        return routeDTO;
    }

    @Override
    public RouteDTO calculJourneyPieton(JourneyDTO journeyDTO, CoordonateDTO coordonateDTO) {
        JSONObject response = apiCaller.callGeoservice(coordonateDTO.getCoordonateFrom(), coordonateDTO.getCoordonateTo(), "Pieton");
        RouteDTO routeDTO = getGeoserviceRoute(response);
        routeDTO.setType(TypeEnum.PIETON);
        routeDTO.setFrom(journeyDTO.getFrom());
        routeDTO.setTo(journeyDTO.getTo());
        routeDTO.setCoDeuxEmmission(0.009);
        return routeDTO;
    }

    @Override
    public RouteDTO calculJourneyVoiture(JourneyDTO journeyDTO, CoordonateDTO coordonateDTO) {
        JSONObject response = apiCaller.callGeoservice(coordonateDTO.getCoordonateFrom(), coordonateDTO.getCoordonateTo(), "Voiture");
        RouteDTO routeDTO = getGeoserviceRoute(response);
        routeDTO.setType(TypeEnum.VOITURE);
        routeDTO.setFrom(journeyDTO.getFrom());
        routeDTO.setTo(journeyDTO.getTo());
        routeDTO.setCoDeuxEmmission(118.5);
        return routeDTO;
    }

    RouteDTO getNavitiaRoute(JSONObject route) {

        RouteDTO routeDTO = new RouteDTO();
        Map<String, RouteDTO> routeDTOMap = new HashMap<>();
        String duration = "";
        if (route != null && route.has("journeys")) {
            JSONArray routes = route.getJSONArray("journeys");
            JSONObject journey;
            for (int i = 0; i < routes.length(); i++) {
                journey = routes.getJSONObject(i);
                logger.info(journey.getString("type"));
                if (Constants.BEST.equals(journey.getString("type")) || Constants.FASTEST.equals(journey.getString("type"))) {
                    try {
                        routeDTO.setStartDateTime(Date.from(Instant.now()));
                        SimpleDateFormat sdf = new SimpleDateFormat(Constants.COMPLETE_DATE_FORMAT);
                        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                        duration = new SimpleDateFormat("HH:mm:ss").format(new Date(TimeUnit.SECONDS.toMillis(journey.getInt("duration"))));
                        Date date = sdf.parse(Constants.DEFAULT_DATE + duration);
                        routeDTO.setDuration(date.getTime());
                        routeDTO.setArrivalDateTime(new Date(routeDTO.getStartDateTime().getTime() + date.getTime()));

                        if (journey.has("co2_emission")) {
                            routeDTO.setCoDeuxEmmission(journey.getJSONObject("co2_emission").getDouble("value"));
                        }
                        List<StepDTO> stepDTOList = new ArrayList<>();
                        if (journey.has("sections")) {
                            JSONArray sections = journey.getJSONArray("sections");
                            for (int n = 0; n < sections.length(); n++) {
                                JSONObject section = sections.getJSONObject(n);
                                if (!Constants.PUBLIC_TRANSPORT.equals(section.getString("type"))) {
                                    if (section.has("path")) {
                                        JSONArray paths = section.getJSONArray("path");
                                        Iterator it = paths.iterator();
                                        while (it.hasNext()) {
                                            StepDTO stepDTO = new StepDTO();
                                            JSONObject step = (JSONObject) it.next();
                                            stepDTO.setDistance(String.valueOf(step.getInt("length")));
                                            stepDTO.setName(step.getString("name"));
                                            stepDTOList.add(stepDTO);
                                        }
                                    }
                                } else {
                                    StepDTO stepDTO = new StepDTO();
                                    JSONObject from = section.getJSONObject("from");
                                    JSONObject to = section.getJSONObject("to");
                                    stepDTO.setDistance("To stop area : " + to.getString("name"));
                                    stepDTO.setName(from.getString("name"));
                                    stepDTOList.add(stepDTO);
                                }
                            }
                        }
                        routeDTO.setRoute(stepDTOList);
                        routeDTOMap.put(journey.getString("type"), routeDTO);
                        routeDTO = new RouteDTO();
                    } catch (ParseException e) {
                        logger.severe(Constants.NOT_PARSABLE + duration + "\n" + e.getMessage());
                    }
                }
            }
        }
        if (routeDTOMap.containsKey(Constants.FASTEST)) {
            return routeDTOMap.get(Constants.FASTEST);
        } else if (routeDTOMap.containsKey(Constants.BEST)) {
            return routeDTOMap.get(Constants.BEST);
        }
        return routeDTO;
    }

    RouteDTO getGeoserviceRoute(JSONObject route) {

        RouteDTO routeDTO = new RouteDTO();
        String duration = "";

        try {

            routeDTO.setStartDateTime(Date.from(Instant.now()));
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.COMPLETE_DATE_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            if (route != null && route.has("duration")) {
                duration = route.getString("duration");
            }
            Date date = sdf.parse(Constants.DEFAULT_DATE + duration);
            routeDTO.setDuration(date.getTime());
            routeDTO.setArrivalDateTime(new Date(routeDTO.getStartDateTime().getTime() + date.getTime()));
            if (route.has("legs")) {
                JSONArray legs = route.getJSONArray("legs");
                JSONObject leg = legs.getJSONObject(0);
                List<StepDTO> stepDTOList = new ArrayList<>();
                if (leg.has("steps")) {
                    JSONArray steps = leg.getJSONArray("steps");
                    Iterator it = steps.iterator();
                    while (it.hasNext()) {
                        StepDTO stepDTO = new StepDTO();
                        JSONObject step = (JSONObject) it.next();
                        stepDTO.setDistance(step.getString("distance"));
                        stepDTO.setName(step.getString("name"));
                        stepDTOList.add(stepDTO);
                    }
                }
                routeDTO.setRoute(stepDTOList);
            }

        } catch (ParseException e) {
            logger.severe(Constants.NOT_PARSABLE + duration + "\n" + e.getMessage());
        }

        return routeDTO;
    }

    RouteDTO getCycleStreetRoute(JSONObject route) {

        RouteDTO routeDTO = new RouteDTO();
        String duration = "";
        if (route.has("marker")) {
            JSONArray markers = route.getJSONArray("marker");

            try {
                JSONObject marker;
                marker = markers.getJSONObject(0);
                if (marker.has("@attributes")) {
                    JSONObject attribut = marker.getJSONObject("@attributes");


                    routeDTO.setStartDateTime(Date.from(Instant.now()));
                    SimpleDateFormat sdf = new SimpleDateFormat(Constants.COMPLETE_DATE_FORMAT);
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    if (attribut.has("time")) {
                        duration = new SimpleDateFormat("HH:mm:ss").format(new Date(TimeUnit.SECONDS.toMillis(Long.valueOf(attribut.getString("time")))));
                    }
                    Date date = sdf.parse(Constants.DEFAULT_DATE + duration);
                    routeDTO.setDuration(date.getTime());
                    routeDTO.setArrivalDateTime(new Date(routeDTO.getStartDateTime().getTime() + date.getTime()));

                    List<StepDTO> stepDTOList = new ArrayList<>();
                    StepDTO stepDTO;
                    for (int i = 1; i < markers.length(); i++) {
                        marker = markers.getJSONObject(i);
                        if (marker.has("@attributes")) {
                            attribut = marker.getJSONObject("@attributes");
                            stepDTO = new StepDTO();
                            if (attribut.has("distance")) {
                                stepDTO.setDistance(attribut.getString("distance"));
                                if (attribut.has("name")) {
                                    stepDTO.setName(attribut.getString("name"));
                                }
                                stepDTOList.add(stepDTO);
                            }
                        }
                    }
                    routeDTO.setRoute(stepDTOList);
                }
            } catch (ParseException e) {
                logger.severe(Constants.NOT_PARSABLE + duration + "\n" + e.getMessage());
            }
        }

        return routeDTO;
    }

    String getCoordonates(JSONArray responses, String apiName) {
        String coordonate = "";
        JSONObject response = (JSONObject) responses.get(0);
        if (response.has("lon")) {
            coordonate += response.getString("lon");
        }
        if (response.has("lat")) {
            coordonate += ((apiName != null && Constants.NAVITIA_NAME.equals(apiName)) ? ";" : ",") + response.getString("lat");
        }
        return coordonate;
    }

    Map<String, CoordonateDTO> getCoordonates(JourneyDTO journeyDTO) {
        Map<String, CoordonateDTO> coordonateDTOMap = new HashMap<>();
        CoordonateDTO coordonateDTONavitia = new CoordonateDTO();
        CoordonateDTO coordonateDTOGeoService = new CoordonateDTO();
        try {
            JSONArray response = apiCaller.callLocationIQ(journeyDTO.getFrom());
            coordonateDTOGeoService.setCoordonateFrom(getCoordonates(response, Constants.GEOSERVICE_NAME));
            coordonateDTONavitia.setCoordonateFrom(getCoordonates(response, Constants.NAVITIA_NAME));
            response = apiCaller.callLocationIQ(journeyDTO.getTo());
            coordonateDTOGeoService.setCoordonateTo(getCoordonates(response, Constants.GEOSERVICE_NAME));
            coordonateDTONavitia.setCoordonateTo(getCoordonates(response, Constants.NAVITIA_NAME));
            coordonateDTOMap.put(Constants.NAVITIA_NAME, coordonateDTONavitia);
            coordonateDTOMap.put(Constants.GEOSERVICE_NAME, coordonateDTOGeoService);
        } catch (NotFoundException e) {
            throw new NotFoundException("Coordonates not found", e);
        }
        return coordonateDTOMap;
    }

}
