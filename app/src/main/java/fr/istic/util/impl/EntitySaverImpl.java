package fr.istic.util.impl;

import fr.istic.domain.Journey;
import fr.istic.domain.Travel;
import fr.istic.dto.RouteDTO;
import fr.istic.dto.UserDTO;
import fr.istic.mapper.TravelMapper;
import fr.istic.mapper.UserMapper;
import fr.istic.repository.JourneyRepository;
import fr.istic.repository.TravelRepository;
import fr.istic.util.EntitySaver;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class EntitySaverImpl implements EntitySaver {

    @Inject
    TravelRepository travelRepository;

    @Inject
    JourneyRepository journeyRepository;

    @Inject
    TravelMapper travelMapper;

    @Inject
    UserMapper userMapper;

    @Override
    public void saveRoute(RouteDTO routeDTO, UserDTO userDTO) {
        Travel travel;
        Journey journey;
        if(routeDTO != null) {
            journey = journeyRepository.findByStartAndDestination(routeDTO.getFrom(),routeDTO.getTo());
            if (journey == null) {
                journey = new Journey();
                journey.setStart(routeDTO.getFrom());
                journey.setDestination(routeDTO.getTo());
                journeyRepository.save(journey);
            }
            travel = travelMapper.toEntity(routeDTO);
            travel.setStart(journey.getStart());
            travel.setDestination(journey.getDestination());
            travel.setUser(userMapper.toEntity(userDTO));
            travelRepository.save(travel);
        }
    }
}
