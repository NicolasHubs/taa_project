package fr.istic.util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;

public interface APICaller {

    JSONObject callGeoservice(String from, String to, String type);

    JSONArray callLocationIQ(String adress);

    JSONObject callNavitia(String from, String to, String type, Instant instant);

    JSONObject callCycleStreet(String from, String to);
}
