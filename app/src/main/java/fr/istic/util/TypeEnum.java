package fr.istic.util;

public enum TypeEnum {
    VOITURE("Voiture", "Voiture"),
    VELO("Velo", "Vélo"),
    VELO_LIBRE_SERVICE("VLS", "Vélo libre service"),
    TRANSPORT_EN_COMMUNS("TEC", "Transports en communs"),
    PIETON("Pieton", "Piéton");

    private String key;
    private String value;

    TypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
