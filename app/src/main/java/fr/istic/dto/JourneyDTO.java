package fr.istic.dto;

import java.util.Objects;

public class JourneyDTO {

    private Long id;
    private String from;
    private String to;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof JourneyDTO)) {
            return false;
        }

        JourneyDTO other = (JourneyDTO) obj;

        return Objects.equals(other.from, this.from)
                && Objects.equals(other.to, this.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.from,this.to);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
