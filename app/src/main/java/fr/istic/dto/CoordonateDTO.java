package fr.istic.dto;

public class CoordonateDTO {

    private String coordonateFrom;

    private String coordonateTo;

    public String getCoordonateFrom() {
        return coordonateFrom;
    }

    public void setCoordonateFrom(String coordonateFrom) {
        this.coordonateFrom = coordonateFrom;
    }

    public String getCoordonateTo() {
        return coordonateTo;
    }

    public void setCoordonateTo(String coordonateTo) {
        this.coordonateTo = coordonateTo;
    }
}
