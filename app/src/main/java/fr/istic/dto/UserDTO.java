package fr.istic.dto;

import java.util.Objects;

public class UserDTO {

    private Long id;
    private String pseudo;
    private String favoriteConveyance;
    private String homeAddress;
    private String workAddress;
    private String adresseMail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFavoriteConveyance() {
        return favoriteConveyance;
    }

    public void setFavoriteConveyance(String favoriteConveyance) {
        this.favoriteConveyance = favoriteConveyance;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserDTO)) {
            return false;
        }

        UserDTO other = (UserDTO) obj;

        return Objects.equals(other.pseudo, this.pseudo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.pseudo,this.adresseMail,this.homeAddress);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", pseudo='" + pseudo + '\'' +
                ", favoriteConveyance='" + favoriteConveyance + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", adresseMail='" + adresseMail + '\'' +
                '}';
    }
}
