package fr.istic.dto;

import java.util.List;
import java.util.Objects;

public class RouteDTO extends TravelDTO {

    private String from;
    private String to;
    private List<StepDTO> route;

    public List<StepDTO> getRoute() {
        return route;
    }

    public void setRoute(List<StepDTO> route) {
        this.route = route;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RouteDTO)) {
            return false;
        }

        RouteDTO other = (RouteDTO) obj;

        return Objects.equals(other.route, this.route);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.route);
    }

    @Override
    public String toString() {
        return super.toString() + "route = { " + route + " }";
    }
}
