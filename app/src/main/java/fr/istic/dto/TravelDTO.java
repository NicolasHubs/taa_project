package fr.istic.dto;

import fr.istic.util.TypeEnum;

import java.util.Date;
import java.util.Objects;

public class TravelDTO {

    private Long id;
    private String start;
    private String destination;
    private Date startDateTime;
    private Date arrivalDateTime;
    private TypeEnum type;
    private long duration;
    private double coDeuxEmmission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public Date getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Date arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public double getCoDeuxEmmission() {
        return coDeuxEmmission;
    }

    public void setCoDeuxEmmission(double coDeuxEmmission) {
        this.coDeuxEmmission = coDeuxEmmission;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TravelDTO)) {
            return false;
        }

        TravelDTO other = (TravelDTO) obj;

        return Objects.equals(other.startDateTime, this.startDateTime)
                && Objects.equals(other.arrivalDateTime, this.arrivalDateTime)
                && Objects.equals(other.type, this.type)
                && Objects.equals(other.duration, this.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.startDateTime, this.arrivalDateTime, this.type, this.duration);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", startDateTime='" + startDateTime + '\'' +
                ", arrivalDateTime='" + arrivalDateTime + '\'' +
                ", type='" + type + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}
