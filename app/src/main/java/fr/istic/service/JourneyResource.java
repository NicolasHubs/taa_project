package fr.istic.service;

import fr.istic.dto.JourneyDTO;

import javax.ws.rs.core.Response;

public interface JourneyResource {

    Response getJourney(JourneyDTO journeyDTO);

    Response postJourney(JourneyDTO journeyDTO, String type);
}