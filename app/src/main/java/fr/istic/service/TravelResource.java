package fr.istic.service;

import javax.ws.rs.core.Response;

public interface TravelResource {

    Response getTravels();
}
