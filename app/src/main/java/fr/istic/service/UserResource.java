package fr.istic.service;

import fr.istic.dto.UserDTO;

import javax.ws.rs.core.Response;

public interface UserResource {

    Response getUser();

    Response postUser(UserDTO userDTO);

}
