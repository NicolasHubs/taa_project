package fr.istic.service.impl;

import fr.istic.domain.Travel;
import fr.istic.dto.TravelDTO;
import fr.istic.mapper.TravelMapper;
import fr.istic.repository.TravelRepository;
import fr.istic.service.TravelResource;
import org.keycloak.KeycloakSecurityContext;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/travel")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TravelResourceImpl implements TravelResource {

    @Inject
    KeycloakSecurityContext keycloakSecurityContext;
    @Inject
    TravelRepository travelRepository;
    @Inject
    TravelMapper travelMapper;

    @GET
    @RolesAllowed({"user", "admin"})
    @Override
    public Response getTravels() {
        List<Travel> travels = travelRepository.findByAdresseMail(keycloakSecurityContext.getToken().getEmail());
        List<TravelDTO> travelDTOList = travelMapper.travelsToTravelDtos(travels);
        return Response.ok(travelDTOList).build();
    }
}
