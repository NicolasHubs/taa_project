package fr.istic.service.impl;

import fr.istic.domain.User;
import fr.istic.dto.UserDTO;
import fr.istic.mapper.UserMapper;
import fr.istic.repository.UserRepository;
import fr.istic.service.UserResource;
import org.keycloak.KeycloakSecurityContext;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/api/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResourceImpl implements UserResource {

    Logger logger = Logger.getLogger(UserResourceImpl.class.getName());

    @Inject
    KeycloakSecurityContext keycloakSecurityContext;
    @Inject
    UserRepository userRepository;
    @Inject
    UserMapper userMapper;

    @GET
    @RolesAllowed({"user","admin"})
    @Override
    public Response getUser(){
        User user = userRepository.findByAdresseMail(keycloakSecurityContext.getToken().getEmail());
        if (user != null) {
            return Response.ok(userMapper.toDto(user)).build();
        }
        return Response.noContent().build();
    }

    @POST
    @RolesAllowed({"admin", "user"})
    @Override
    public Response postUser(UserDTO userDTO) {
        if (userDTO != null && userDTO.getAdresseMail() != null) {
            User user = userMapper.toEntity(userDTO);
            userRepository.saveOrUpdate(user);
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
