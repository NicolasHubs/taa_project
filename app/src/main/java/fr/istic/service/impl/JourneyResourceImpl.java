package fr.istic.service.impl;

import fr.istic.dto.JourneyDTO;
import fr.istic.dto.RouteDTO;
import fr.istic.dto.UserDTO;
import fr.istic.mapper.UserMapper;
import fr.istic.repository.JourneyRepository;
import fr.istic.repository.UserRepository;
import fr.istic.service.JourneyResource;
import fr.istic.util.EntitySaver;
import fr.istic.util.JourneyCalculator;
import fr.istic.util.TypeEnum;
import org.keycloak.KeycloakSecurityContext;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.logging.Logger;

@Path("/api/journey")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JourneyResourceImpl implements JourneyResource {

    Logger logger = Logger.getLogger(JourneyResourceImpl.class.getName());

    @Inject
    KeycloakSecurityContext keycloakSecurityContext;

    @Inject
    JourneyCalculator journeyCalculator;

    @Inject
    EntitySaver entitySaver;

    @Inject
    JourneyRepository journeyRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    UserMapper userMapper;

    @GET
    @RolesAllowed({"user","admin"})
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getJourney(JourneyDTO journeyDTO) {
        if(journeyDTO != null) {
            journeyRepository.findByStartAndDestination(journeyDTO.getFrom(), journeyDTO.getTo());
            return Response.ok().build();
        }
        return Response.noContent().build();
    }

    @POST
    @RolesAllowed({"user","admin"})
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response postJourney(JourneyDTO journeyDTO, @QueryParam("type") String type) {
        UserDTO userDTO = userMapper.toDto(userRepository.findByAdresseMail(keycloakSecurityContext.getToken().getEmail()));
        logger.info(type);
        if (journeyDTO != null && type != null && !type.isEmpty()) {
            if(Arrays.stream(TypeEnum.values()).filter(e -> e.getKey().equals(type)).count() > 0) {
                try {
                    RouteDTO route = journeyCalculator.calculJourney(journeyDTO, type);
                    if (route.getArrivalDateTime() != null) {
                        entitySaver.saveRoute(route, userDTO);
                    }
                    return Response.ok(route).build();
                } catch (NotFoundException e) {
                    logger.severe("Coordonates not found");
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
            } else {
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            }
        }
        return Response.noContent().build();
    }
}
