package fr.istic.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class User extends PanacheEntity {

    @Column(nullable = false, unique = true)
    private String adresseMail;

    private String favoriteConveyance;

    private String homeAddress;

    private String workAddress;

    private String pseudo;

    @OneToMany(mappedBy="user")
    private Collection<Travel> travels;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFavoriteConveyance() { return favoriteConveyance; }

    public void setFavoriteConveyance(String favoriteConveyance) { this.favoriteConveyance = favoriteConveyance; }

    public String getHomeAddress() { return homeAddress; }

    public void setHomeAddress(String homeAddress) { this.homeAddress = homeAddress; }

    public String getWorkAddress() { return workAddress; }

    public void setWorkAddress(String workAddress) { this.workAddress = workAddress; }

    public String getAdresseMail() { return adresseMail; }

    public void setAdresseMail(String adresseMail) { this.adresseMail = adresseMail; }

    public Collection<Travel> getTravels() {
        return travels;
    }

    public void setTravels(Collection<Travel> travels) {
        this.travels = travels;
    }
}
