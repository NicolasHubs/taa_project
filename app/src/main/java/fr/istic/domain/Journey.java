package fr.istic.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Journey extends PanacheEntity {

    private String start;

    private String destination;

    //@OneToMany(mappedBy="journey")
    //private Collection<Travel> travels ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    /*public Collection<Travel> getTravels() {
        return travels;
    }

    public void setTravels(Collection<Travel> travels) {
        this.travels = travels;
    }*/
}
