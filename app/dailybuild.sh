#tagversion=$(git tag -l --sort=-version:refname "release-*" | head -n 1 | grep -Eo '[0-9]+$')
tagversion=$(git tag -l --sort=-version:refname "release-*" | head -n 1 | cut -d'-' -f2)
if [ -z "$tagversion" ]
then
	tagversion="release-0.0.1"
else
	newversion=$(($(echo "$tagversion" | grep -Eo '[0-9]+$') + 1))
	tagversion="release-$(echo "$tagversion" | sed 's/[0-9]\+$//')$newversion"
fi
echo "New tag version -> $tagversion"
git tag $tagversion
git push --tags
