none_im_list=$(docker images -f 'dangling=true' -q)
if [[ $? != 0 ]]; then
	echo "Hhmm it seems that the previous command faced a problem. (docker image -f 'dangling=true' -q)"
elif [[ $none_im_list ]]; then
    	docker rmi -f ${none_im_list}
else
        echo "No <none> images to remove"
fi
