import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {HasAnyAuthorityDirective} from './has-any-authority.directive';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {GeneratetravelComponent} from './entities/generatetravel/generatetravel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavbarComponent} from './entities/navbar/navbar.component';
import {SetProfileComponent} from './entities/set-profile/set-profile.component';
import {MyProfileComponent} from './entities/my-profile/my-profile.component';
import {MyTravelComponent} from './entities/my-travel/my-travel.component';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {TravelPathsComponent} from './entities/travel-paths/travel-paths.component';

import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {CardelementComponent} from './entities/cardelement/cardelement.component';
import {MatListModule} from '@angular/material/list';
import {NgCircleProgressModule} from 'ng-circle-progress';

const keycloakService = new KeycloakService();

@NgModule({
  declarations: [
    AppComponent,
    HasAnyAuthorityDirective,
    GeneratetravelComponent,
    NavbarComponent,
    SetProfileComponent,
    MyProfileComponent,
    MyTravelComponent,
    TravelPathsComponent,
    CardelementComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    KeycloakAngularModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatCardModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatListModule,
    NgCircleProgressModule.forRoot({
      backgroundPadding: 7,
      radius: 60,
      space: -2,
      outerStrokeWidth: 2,
      outerStrokeColor: '#808080',
      innerStrokeColor: '#e7e8ea',
      innerStrokeWidth: 2,
      showSubtitle: true,
      title: [
        'working',
        'in',
        'progress'
      ],
      titleFontSize: '12',
      subtitleFontSize: '20',
      animateTitle: false,
      animationDuration: 1000,
      showUnits: false,
      clockwise: false
    })
  ],
  providers: [
    {
      provide: KeycloakService,
      useValue: keycloakService
    }
  ],
  // bootstrap: [AppComponent],
  entryComponents: [AppComponent]

})
export class AppModule {

  // noinspection JSUnusedGlobalSymbols
  ngDoBootstrap(app) {
    function isTokenExpired() {
      return Date.now() >= (parseInt(localStorage.tokencreationdate) + parseInt(localStorage.tokenduration));
    }

    if (localStorage.kc_token !== 'undefined' && localStorage.kc_refreshToken !== 'undefined' && !isTokenExpired()) {
      const token = localStorage.kc_token;
      const refreshToken = localStorage.kc_refreshToken;

      keycloakService
        .init({
          initOptions: {
            token,
            refreshToken,
          },
          bearerExcludedUrls: ['locationiq*']
        })
        .then(() => {
          console.log('[ngDoBootstrap] bootstrap app - With token');
          localStorage.kc_token = keycloakService.getKeycloakInstance().token;
          localStorage.kc_refreshToken = keycloakService.getKeycloakInstance().refreshToken;
          app.bootstrap(AppComponent);
        })
        .catch(error => console.error('[ngDoBootstrap] init Keycloak failed', error));
    } else {
      keycloakService
        .init({
          bearerExcludedUrls: ['locationiq*']
        })
        .then(() => {
          console.log('[ngDoBootstrap] bootstrap app - Without token');
          localStorage.kc_token = keycloakService.getKeycloakInstance().token;
          localStorage.kc_refreshToken = keycloakService.getKeycloakInstance().refreshToken;
          localStorage.tokenduration = 60 * 1000;
          localStorage.tokencreationdate = Date.now();
          app.bootstrap(AppComponent);
        })
        .catch(error => console.error('[ngDoBootstrap] init Keycloak failed', error));
    }
  }
}
