export class Step {
  name: string;
  distance: string;

  constructor(name: string, distance: string) {
    this.name = name;
    this.distance = distance;
  }
}
