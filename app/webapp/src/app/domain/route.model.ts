import {Step} from './step.model';
import {TypeEnum} from './enum/type.model';

export class Route {
  from: string;
  to: string;
  startDateTime: Date;
  arrivalDateTime: Date;
  type: TypeEnum;
  duration: number;
  coDeuxEmmission: number;
  route: Step[];

  constructor(from: string, to: string, startDateTime: Date,
              arrivalDateTime: Date, type: TypeEnum, duration: number, coDeuxEmmission: number, route: Step[]) {
    this.from = from;
    this.to = to;
    this.startDateTime = startDateTime;
    this.arrivalDateTime = arrivalDateTime;
    this.type = type;
    this.duration = duration;
    this.coDeuxEmmission = coDeuxEmmission;
    this.route = route;
  }
}
