import {TypeEnum} from "./enum/type.model";

export class Travel {
  id: number;
  start: String;
  destination: String;
  startDateTime: Date;
  arrivalDateTime: Date;
  startDateRepresentation: String;
  arrivalDateRepresentation: String;
  type: TypeEnum;
  typeRepresentation: String;
  duration: number;
  coDeuxEmmission : number;

  constructor(id: number, start: string, destination: string,startDateTime: Date, arrivalDateTime: Date, type: TypeEnum, duration: number, coDeuxEmmission: number) {
    this.id = id;
    this.start = start;
    this.destination = destination;
    this.startDateTime = startDateTime;
    this.arrivalDateTime = arrivalDateTime;
    this.type = type;
    this.duration = duration;
    this.coDeuxEmmission = coDeuxEmmission;
  }
}
