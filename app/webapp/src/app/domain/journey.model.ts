export class Journey {
  id: number;
  from: string;
  to: string;

  constructor(id: number, from: string, to: string) {
    this.id = id;
    this.from = from;
    this.to = to;
  }
}
