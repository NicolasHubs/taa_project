export enum TypeEnum {
  VOITURE = 'Voiture',
  VELO = 'Velo',
  TRANSPORT_EN_COMMUNS = 'TEC',
  PIETON = 'Pieton',
}

// tslint:disable-next-line:no-namespace
export namespace TypeEnum {

  export function keys() {
    return Object.keys(TypeEnum).filter(
      (type) => isNaN(type as any) && type !== 'values' && type !== 'keys'
    );
  }
}
