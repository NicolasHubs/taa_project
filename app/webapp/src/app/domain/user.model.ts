export class User {
  pseudo: string;
  favoriteConveyance: string;
  homeAddress: string;
  workAddress: string;
  adresseMail: string;

  constructor(pseudo: string, favoriteConveyance: string, homeAddress: string, workAddress: string, adresseMail: string) {
    this.pseudo = pseudo;
    this.favoriteConveyance = favoriteConveyance;
    this.homeAddress = homeAddress;
    this.workAddress = workAddress;
    this.adresseMail = adresseMail;
  }
}
