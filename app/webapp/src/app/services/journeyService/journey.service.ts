import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import {Journey} from '../../domain/journey.model';
import {TypeEnum} from '../../domain/enum/type.model';
import {catchError, delay, map} from 'rxjs/operators';


const endpoint = 'http://localhost:8080/api/';
const loncationIq = 'https://api.locationiq.com/v1/autocomplete.php?key=e4559681aca0b1&q=';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class JourneyService {

  constructor(private http: HttpClient) {
  }

  getJourneys(): void {
    console.warn('Not implemented');
    /*
    return this.http.get(endpoint + 'products').pipe(
      map(this.extractData));
     */
  }

  getJourney(journey: Journey): Observable<Journey> {
    let params = new HttpParams();

    if (journey.from != null && journey.to != null) {
      params = params.append('from', journey.from);
      params = params.append('to', journey.to);
    }
    return this.http.get<Journey>(endpoint + 'journey', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }), params
    }).pipe();
  }

  search(research: string): Observable<any> {
    return this.http.get(loncationIq + research).pipe(
      map((response: any[]) => response.map((item: any) => {
          return item.display_name;
        })
      ),
      catchError(err => {
        return '';
      }));
  }

  postJourney(journey: Journey, type: TypeEnum): Observable<any> {
    const body = {from: journey.from, to: journey.to};
    return this.http.post(endpoint + 'journey?type=' + type, body, httpOptions).pipe(
      delay(5000),
      map((response: any) => {
          if (response.route == null) {
            throw new Error('No duration');
          } else {
            return response;
          }
        }
      ),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  addJourney(product): void {
    console.warn('Not implemented');
    /*
    console.log(product);
    return this.http.post<any>(endpoint + 'products', JSON.stringify(product), httpOptions).pipe(
      tap((product) => console.log(`added product w/ id=${product.id}`)),
      catchError(this.handleError<any>('addProduct'))
    );*/
  }

  updateJourney(id, product): void {
    console.warn('Not implemented');
    /*
    return this.http.put(endpoint + 'products/' + id, JSON.stringify(product), httpOptions).pipe(
      tap(_ => console.log(`updated product id=s${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );*/
  }

  deleteJourney(id): void {
    console.warn('Not implemented');
    /*
    return this.http.delete<any>(endpoint + 'products/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted product id=${id}`)),
      catchError(this.handleError<any>('deleteProduct'))
    );*/
  }

}
