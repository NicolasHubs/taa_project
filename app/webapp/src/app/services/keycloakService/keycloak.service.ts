import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  // we declare that this service should be created
  // by the root application injector.
  providedIn: 'root',
})
export class MyKeycloakService {
  constructor(private http: HttpClient) {
  }

  SERVER_KEYCLOAK_URL = 'http://localhost:8180';

  public setRoles(roles: any, token: any, userId: string): Observable<any> {
    const httpOptions: { headers; observe } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token
      }),
      observe: 'response'
    };
    return this.http.post(
      this.SERVER_KEYCLOAK_URL + '/auth/admin/realms/quarkus-realm/users/' + userId + '/role-mappings/realm',
      roles,
      httpOptions
    );
  }

  public getId(username: any, token: any): Observable<any> {
    const httpOptions: { headers; observe } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token
      }),
      observe: 'response'
    };
    return this.http.get(this.SERVER_KEYCLOAK_URL + '/auth/admin/realms/quarkus-realm/users?username=' + username, httpOptions);
  }
}
