import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from "@angular/core";
import {User} from "../../domain/user.model";

const endpoint = 'http://localhost:8080/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUser(): Observable<User> {
    return this.http.get<User>(endpoint + 'user', httpOptions).pipe();
  }

  postUser(user: User): Observable<any> {
    const body = {
      adresseMail: user.adresseMail,
      favoriteConveyance: user.favoriteConveyance,
      homeAddress: user.homeAddress,
      workAddress: user.workAddress,
      pseudo: user.pseudo
    };
    return this.http.post(endpoint + 'user?', body, httpOptions).pipe();
  }

}
