import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  // we declare that this service should be created
  // by the root application injector.
  providedIn: 'root',
})
export class MyStorageService {

  isConnectedValue = new Subject();

  public get isConnected(): boolean {
    return localStorage.getItem('isConnected') == 'true';
  }

  public set isConnected(isConnected: boolean) {
    this.isConnectedValue.next(isConnected);
    localStorage.setItem('isConnected', String(isConnected));
  }
}
