import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SetProfileComponent} from './entities/set-profile/set-profile.component';
import {MyProfileComponent} from './entities/my-profile/my-profile.component';
import {MyTravelComponent} from './entities/my-travel/my-travel.component';
import {AuthGuardService} from './services/auth-guard.service';
import {TravelPathsComponent} from './entities/travel-paths/travel-paths.component';
import {JourneyService} from './services/journeyService/journey.service';
import {TravelService} from './services/TravelService/travel.service';
import {UserService} from './services/UserService/user.service';
import {GeneratetravelComponent} from './entities/generatetravel/generatetravel.component';

const routes: Routes = [
  {
    path: '',
    component: GeneratetravelComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'travels',
    component: MyTravelComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'setProfile',
    component: SetProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'myProfile',
    component: MyProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'travelPaths',
    component: TravelPathsComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService,
    JourneyService,
    TravelService,
    UserService]
})

export class AppRoutingModule {
}
