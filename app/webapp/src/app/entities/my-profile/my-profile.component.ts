import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../services/UserService/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../domain/user.model';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  profilForm: FormGroup;
  currentEmail: string;
  user: User;
  selectedMean: any;

  constructor(private userService: UserService, private fb: FormBuilder,
              private route: ActivatedRoute, private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      user: User,
    };
    this.user = state.user;
  }

  ngOnInit() {
    this.profilForm = this.fb.group({
      pseudo: null,
      homeAdd: null,
      workAdd: null,
      favCov: null
    });

    this.profilForm.get('pseudo').setValue(this.user.pseudo);
    this.profilForm.get('homeAdd').setValue(this.user.homeAddress);
    this.profilForm.get('workAdd').setValue(this.user.workAddress);
    this.profilForm.get('favCov').setValue(this.user.favoriteConveyance);
    this.currentEmail = this.user.adresseMail;

  }

  validateProfil() {
    const userUpdated = new User(this.profilForm.get('pseudo').value, this.profilForm.get('favCov').value,
      this.profilForm.get('homeAdd').value, this.profilForm.get('workAdd').value, this.currentEmail);
    this.userService.postUser(userUpdated).subscribe();
  }

  valid() {
    return this.profilForm.get('pseudo').value != null &&
      this.profilForm.get('favCov').value != null &&
      this.profilForm.get('homeAdd').value != null &&
      this.profilForm.get('workAdd').value != null &&
      this.currentEmail != null &&
      this.profilForm.get('pseudo').value.length > 0 &&
      this.profilForm.get('favCov').value.length > 0 &&
      this.profilForm.get('homeAdd').value.length > 0 &&
      this.profilForm.get('workAdd').value.length > 0 &&
      this.currentEmail.length > 0;
  }

  onChange($event: Event, deviceValue: any) {
    console.log(deviceValue);
  }
}
