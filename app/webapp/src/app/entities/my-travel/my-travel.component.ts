import {Component, OnInit} from '@angular/core';
import {Travel} from '../../domain/travel.model'
import {TravelService} from "../../services/TravelService/travel.service";

@Component({
  selector: 'app-my-travel',
  templateUrl: './my-travel.component.html',
  styleUrls: ['./my-travel.component.css']
})
export class MyTravelComponent implements OnInit {
  travels: Travel[];

  constructor(private travelService: TravelService) {
    this.travelService.getTravels().subscribe((data) =>
      this.travels = data.map((travel) => {
        let date;
        let hour;
        if (travel.startDateTime) {
          date = travel.startDateTime.toString().replace("-", " ").split("T");
          travel.startDateRepresentation = date[0];
          hour = date[1].split("Z")[0];
          travel.startDateRepresentation += " " + hour;
        }
        if (travel.arrivalDateTime) {
          date = travel.arrivalDateTime.toString().replace("-", " ").split("T");
          travel.arrivalDateRepresentation = date[0];
          hour = date[1].split("Z")[0];
          travel.arrivalDateRepresentation += " " + hour;
        }
        if (travel.type != null) {
          if ("TRANSPORT_EN_COMMUNS" == travel.type.toString()) {
            travel.typeRepresentation = "Transports en communs";
          } else if ("PIETON" == travel.type.toString()) {
            travel.typeRepresentation = "Piéton";
          } else if ("VELO" == travel.type.toString()) {
            travel.typeRepresentation = "Vélo";
          } else if ("VOITURE" == travel.type.toString()) {
            travel.typeRepresentation = "Voiture";
          }
        }
        return travel
      })
    )
  }

  ngOnInit() {
  }

}
