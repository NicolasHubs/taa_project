import {Component, Input, OnInit} from '@angular/core';
import {Route} from '../../domain/route.model';

@Component({
  selector: 'app-cardelement',
  templateUrl: './cardelement.component.html',
  styleUrls: ['./cardelement.component.css']
})
export class CardelementComponent implements OnInit {
  @Input() element: Route;

  elementDisplay: string;

  isVisible: boolean;
  @Input() iconName: string;

  constructor() {
    this.isVisible = true;
  }

  ngOnInit() {
  }

  result(): string {
    return this.elementDisplay = this.element.type + ' | Arrival: ' +
      this.displayDate(this.element.arrivalDateTime.toString()) + ' | Duration: ';
  }

  displayDate(date: string): string {
    let dateRepresentation = date.replace("-"," ").split("T");
    return dateRepresentation[0]+" "+dateRepresentation[1].split("Z")[0];
  }

  toggleElement() {
    this.isVisible = !this.isVisible;
  }
}
