import {Component, OnInit} from '@angular/core';
import {JourneyService} from '../../services/journeyService/journey.service';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, switchMap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UserService} from '../../services/UserService/user.service';
import {User} from "../../domain/user.model";

@Component({
  selector: 'app-generatetravel',
  templateUrl: './generatetravel.component.html',
  styleUrls: ['./generatetravel.component.css']
})
export class GeneratetravelComponent implements OnInit {
  locationsFromIQ: Observable<any>;
  locationsToIQ: Observable<any>;
  locationForm: FormGroup;

  constructor(private userService: UserService, private fb: FormBuilder, private snackBar: MatSnackBar,
              private journeyService: JourneyService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.locationForm = this.fb.group({
      fromInput: null,
      toInput: null
    });

    this.locationsFromIQ = this.locationForm.get('fromInput').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => this.journeyService.search(value))
      );
    this.locationsToIQ = this.locationForm.get('toInput').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => this.journeyService.search(value))
      );
  }

  displayFn(adress: string) {
    if (adress) {
      return adress;
    }
  }

  /*Request example let journey = new Journey(null,"Thabor - St Hélier", "14 Rue François Lanno, 35700 Rennes");
  this.journeyService.postJourney(journey,TypeEnum.TRANSPORT_EN_COMMUNS).subscribe((data: Route) => {
    console.log(data);
  });*/

  public validateJourney() {

    const navigationExtras: NavigationExtras = {
      state: {
        from: this.locationForm.get('fromInput').value,
        to: this.locationForm.get('toInput').value
      }
    };

    this.router.navigate(['/travelPaths'], navigationExtras);
  }

  public valid() {
    return this.locationForm.get('fromInput').value != null && this.locationForm.get('toInput').value != null && this.locationForm.get('fromInput').value.length > 0 && this.locationForm.get('toInput').value.length > 0;
  }

  fillWithProfile() {
    this.userService.getUser().subscribe((data: User) => {
      console.log(data);
      this.locationForm.get('fromInput').setValue(data.homeAddress);
      this.locationForm.get('toInput').setValue(data.workAddress);
      this.locationsFromIQ = this.locationForm.get('fromInput').valueChanges
        .pipe(
          debounceTime(300),
          switchMap(value => this.journeyService.search(value))
        );
      this.locationsToIQ = this.locationForm.get('toInput').valueChanges
        .pipe(
          debounceTime(300),
          switchMap(value => this.journeyService.search(value))
        );
    });
  }
}
