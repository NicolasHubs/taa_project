import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MyStorageService} from "../../services/StorageService/my-storage-service";
import {KeycloakService} from "keycloak-angular";
import {User} from "../../domain/user.model";
import {UserService} from "../../services/UserService/user.service";

@Component({
  selector: 'app-profile',
  templateUrl: './set-profile.component.html',
  styleUrls: ['./set-profile.component.css']
})
export class SetProfileComponent implements OnInit {

  pseudoFormGroup: FormGroup;
  homeAddressFormGroup: FormGroup;
  workAddressFormGroup: FormGroup;
  conveyanceFormGroup: FormGroup;

  logged = false;

  constructor(private userService: UserService, private storageService: MyStorageService, private keyCloak: KeycloakService, private formBuidler: FormBuilder) {
    this.logged = storageService.isConnected;
    storageService.isConnectedValue.subscribe((nextValue) => {
      if (typeof nextValue === 'boolean') {
        this.logged = nextValue;
      }
    });
  }

  ngOnInit() {
    this.pseudoFormGroup = this.formBuidler.group({
      pseudo: ['', Validators.required]
    });
    this.homeAddressFormGroup = this.formBuidler.group({
      homeAddress: ['', Validators.required]
    });
    this.workAddressFormGroup = this.formBuidler.group({
      workAddress: ['', Validators.required]
    });
    this.conveyanceFormGroup = this.formBuidler.group({
      conveyance: ['', Validators.required]
    });
  }

  async setUserInfo() {
    await this.keyCloak.loadUserProfile()
      .then(
        (userDetails) => {
          const newUser = new User(this.pseudoFormGroup.get("pseudo").value,
            this.conveyanceFormGroup.get("conveyance").value,
            this.homeAddressFormGroup.get("homeAddress").value,
            this.workAddressFormGroup.get("workAddress").value,
            userDetails.email)
          this.userService.postUser(newUser).subscribe();
        }
      );

  }
}
