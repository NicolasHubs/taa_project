import {Component, OnInit} from '@angular/core';
import {MyStorageService} from '../../services/StorageService/my-storage-service';
import {KeycloakService} from 'keycloak-angular';
import {User} from "../../domain/user.model";
import {UserService} from "../../services/UserService/user.service";
import {NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logged = false;
  user: User;

  constructor(private userService: UserService, private storageService: MyStorageService, private keyCloak: KeycloakService, private router: Router) {
    this.logged = storageService.isConnected;
    storageService.isConnectedValue.subscribe((nextValue) => {
      if (typeof nextValue === 'boolean') {
        this.logged = nextValue;
      }
    });

  }

  ngOnInit(): void {
  }

  getToMyProfile(){
    this.userService.getUser().subscribe((data) => {
      if (data != null) {
        this.user = new User(data.pseudo, data.favoriteConveyance, data.homeAddress, data.workAddress, data.adresseMail);
        const navigationExtras: NavigationExtras = {
          state: {
            user: this.user,
          }
        };
        this.router.navigate(['/myProfile'], navigationExtras);
      } else {
        this.router.navigate(['/setProfile']);
      }
    });
  }

  login() {
    // noinspection JSIgnoredPromiseFromCall
    this.keyCloak.login();
  }

  logout() {
    // noinspection JSIgnoredPromiseFromCall
    this.keyCloak.logout();
  }
}
