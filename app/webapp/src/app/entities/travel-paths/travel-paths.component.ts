import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JourneyService} from '../../services/journeyService/journey.service';
import {Journey} from '../../domain/journey.model';
import {TypeEnum} from '../../domain/enum/type.model';
import {Route} from '../../domain/route.model';

@Component({
  selector: 'app-travel-paths',
  templateUrl: './travel-paths.component.html',
  styleUrls: ['./travel-paths.component.css']
})
export class TravelPathsComponent implements OnInit {

  from: string;
  to: string;

  tecRoute: Route;
  pietonRoute: Route;
  voitureRoute: Route;
  veloRoute: Route;
  percentage: number;

  constructor(private journeyService: JourneyService, private route: ActivatedRoute, private router: Router) {
    this.percentage = 0;
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      from: string,
      to: string
    };

    this.from = state.from;
    this.to = state.to;

    const journey = new Journey(null, this.from, this.to);

    this.getTravels(journey).then(r => console.log('Fini, merci d\'avoir attendu !'));
  }

  public async getTravels(journey: Journey): Promise<void> {
    this.getVOITURE(journey).then(
      (onValue1) => {
        this.voitureRoute = onValue1;
        this.percentage += 25;
        this.getTRANSPORT_EN_COMMUNS(journey).then(
          (onValue2) => {
            this.tecRoute = onValue2;
            this.percentage += 25;
            this.getVELO(journey).then(
              (onValue3) => {
                this.veloRoute = onValue3;
                this.percentage += 25;
                this.getPIETON(journey).then(
                  (onValue4) => {
                    this.pietonRoute = onValue4;
                    this.percentage += 25;
                  });
              });
          });
      });
  }

  private getTRANSPORT_EN_COMMUNS(journey: Journey): Promise<Route> {
    return new Promise((resolve, reject) => {

      setTimeout(() =>
          this.journeyService.postJourney(journey, TypeEnum.TRANSPORT_EN_COMMUNS).subscribe(async (data: Route) => {
              resolve(data);
            },
            async error => {
              resolve(null);
            })
        , 2000);
    });
  }

  private getPIETON(journey: Journey): Promise<Route> {
    return new Promise((resolve, reject) => {
      setTimeout(() =>
          this.journeyService.postJourney(journey, TypeEnum.PIETON).subscribe(async (data: Route) => {
              resolve(data);
            },
            async error => {
              resolve(null);
            })
        , 2000);
    });
  }

  private getVOITURE(journey: Journey): Promise<Route> {
    return new Promise((resolve, reject) => {
      this.journeyService.postJourney(journey, TypeEnum.VOITURE).subscribe(async (data: Route) => {
          resolve(data);
        },
        async error => {
          resolve(null);
        })
    });
  }

  private getVELO(journey: Journey): Promise<Route> {
    return new Promise((resolve, reject) => {
      setTimeout(() =>
          this.journeyService.postJourney(journey, TypeEnum.VELO).subscribe(async (data: Route) => {
              resolve(data);
            },
            async error => {
              resolve(null);
            })
        , 2000);
    });
  }

  ngOnInit() {
  }

  percentageValue() {
    return this.percentage + '%';
  }
}
