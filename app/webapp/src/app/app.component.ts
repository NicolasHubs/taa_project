import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';
import { MyStorageService } from './services/StorageService/my-storage-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'lastarbis';
  logged: boolean;

  constructor(private http: HttpClient, private keyCloak: KeycloakService, private storageService: MyStorageService) {
    this.logged = storageService.isConnected;
    storageService.isConnectedValue.subscribe((nextValue) => {
      if (typeof nextValue === 'boolean') {
        this.logged = nextValue;
      }
    });
  }

  ngOnInit(): void {
    this.keyCloak.isLoggedIn().then(e => {
      this.storageService.isConnected = e;
    });
    console.log(this.keyCloak.getUserRoles());
  }
}
