(The $CI_GIT_TOKEN expires the 31/10/2019)

# TAA_Project

Bus, Métro, Voiture personnelle, VeloStar, le choix des moyens de transports sur Rennes est vaste pour effectuer son trajet domicile-travail. L’objectif du projet est de regarder comment un utilisateur, en agrégeant les données fournies de manière ouverte par Rennes métropole, Keolis, ou d’autres acteurs pourrait demander chaque matin ou chaque soir, quel est le moyen de transport le plus rapide.
Votre application doit donc permettre de gérer un ensemble d’utilisateurs qui renseignent chacun son lieu de domicile et son lieu de travail, les stations de vélos proches de ces lieux et les arrêts de bus associés à ces trajets afin de déterminer en temps réel le temps de transport moyen pour les différents modes de transports.


Configure properly [GitLab with SSH keys](https://docs.gitlab.com/ee/ssh/), then clone the project :
```bash
git clone git@gitlab.com:NicolasHubs/taa_project.git
```

## Intellij configuration

Install [Intellij](https://www.jetbrains.com/idea/download/download-thanks.html?platform=linux)

## Keycloak configuration

To build the application you need to have keycloak properly set up.

Start keycloak (via docker-compose, docker or from a standalone installation):

1. Go to 'http://localhost:8180' (or from a different port depending on your configuration)
2. Administration Console->(user: 'admin', psswd: 'admin')->Add realm->(Select file: 'taa_project/config/keycloak/quarkus-realm.json', name: 'quarkus-realm')->Create
3. Configure->Clients->Create->(Client ID: 'quarkus-app', Root URL: 'http://localhost:8080')->Save
4. Configure->Clients->quarkus-app->Settings->(Switch 'Access Type' from 'public' to 'confidential')->Save
5. Configure->Clients->quarkus-app->Installation->(Format Option: 'Keycloak OIDC JSON')->Download and Save file in 'taa_project/app/src/main/resources/keycloak.json'

Then you would be able to compile the quarkus app with './mvnw compile quarkus:dev' and access the 'http://localhost:8080' without errors.

## GraalVM configuration

In order to produce a native executable for the application, you need to install GraalVM :

1. Download [GraalVM](https://github.com/oracle/graal/releases/tag/vm-19.1.1)
2. Extract it in your '$HOME/Development/graalvm/' directory
3. Execute the following linux commands :
```bash
export GRAALVM_HOME=$HOME/Development/graalvm/
sudo apt-get install build-essential libz-dev
# Now you must reload terminal
cd $HOME/Development/graalvm/bin
./gu install native-image
```

## Launch application

Launch the different modules from the docker-compose :
```bash
./launch_app.sh
```

Generate the jar (make sure you have enough ram available (at least 4go))
```bash
mvn package -Pnative -DskipTests -Dmaven.skip.test
```

Finally, each time you want to launch the app, use :
```bash
./target/code-with-quarkus-1.0.0-SNAPSHOT-runner
```
## Front angular

L'utilisateur peut se connecter, se déconnecter, ainsi que créer un compte via keycloak.

![alt text](img/se_connecter.png?raw=true "photo")

Une fois connecté, l'utilisateur se retrouve sur la page de génération automatique de trajets, dans lequel il
devra renseigner l'adresse de départ (-> from) et celle d'arrivée (-> to). Il a aussi la possibilitée de remplir
automatiquement ces champs avec ses préférences associées à son profil. 

![alt text](img/generer_trajet.png?raw=true "photo")

![alt text](img/generer_trajet_result.png?raw=true "photo")

Il peut changer les préférences associées à son profil dans l'onglet "My profile". Si le profil de l'utilisateur n'a
pas encore été complété, il sera redirigé vers une page où il devra renseigner successivement : pseudo, adresse du domicile,
adresse du travail, moyen de transport favori.

![alt text](img/remplir_son_profil.png?raw=true "photo")

Si son profil est déjà rempli, l'utilisateur sera redirigé vers une page récapitulative de son profil où il aura la 
possibilité de modifier indépendemment chacun des champs.

![alt text](img/consulter_profil.png?raw=true "photo")

Si l'utilisateur est pris d'une quelconque nostalgie, il pourra s'il le souhaite, consulter l'historique de ses précédentes
demandes de trajets.

![alt text](img/consulter_historique.png?raw=true "photo")

## Back quarkus

### Application Properties

Le serveur quarkus est configuré pour persister ses données dans la base mariaDB lancée via docker-compose, et pour être capable de communiquer avec keycloak afin de s'assurer que les tokens aient bien été créé par la même instance de Keycloak.

Il expose également un swagger via cette url : [http://localhost:8080/swagger-ui/]

Ce dernier étant configuré pour demander un token pour exécuter les requêtes via le fichier /resources/META-INF/openapi.yml

### Dockerisation

Quarkus propose deux DockerFile permettant de lancer le serveur soit en JVM mode soit en natif.

### Persistance

#### Entities

Les entités sont des entités Panache, utilisant les annotations @OneToMany et @ManyToOne.

#### Repositories

Les repositories sont des repositories Panache, qui permettent d'utiliser des méthodes tel que find pour construire des requêtes facilement, ou, au besoin, de définir des requêtes complexes de type HQL.

#### DTO & mappers

Afin d'exposer les données via les services REST, ces dernières sont converties en DTO à l'aide de mapper. Ces mappers sont des interfaces qui vont ensuite être implémenté par quarkus à la compilation.

### Services

Le service Journey est le point proposant un calcul d'itinéraire entre deux adresses, pour 4 types de véhicules [Voiture, Transports en commun, Vélo, Piéton]

Chaque itinéraire est représenté par :

* start: L'adresse du lieu de départ
* destination: L'adresse du lieu d'arrivée
* startDateTime: La date et l'heure de la requête
* arrivalDateTime: L'estimation de la date et de l'heure d'arrivée
* type: Le moyen de transport
* duration: La durée estimée du trajet
* coDeuxEmmission : L'estimation de l'émission de CO<sub>2</sub>

Le service Travel permet de récupérer l'historique des recherches d'itinéraire de l'utilisateur.

Le service User permet de créer et modifier le profil de l'utilisateur.

### API externes

Afin de calculer les différents itinéraires, différentes API sont utilisées :

* LocationIQ : cette API permet de récupérer les coordonnées correspondant aux adresses fournies lors de l'appel, car la majorité des API calcul des itinéraires entre coordonnées

* GeoService : cette API permet de calculer les itinéraires piétions et pour les voitures

* Navitia : pour les transports en commun

* CycleStreet : pour les itinéraires vélo